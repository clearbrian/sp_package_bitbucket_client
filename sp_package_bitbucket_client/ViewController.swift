//
//  ViewController.swift
//  sp_package_bitbucket_client
//
//  Created by Brian Clear on 13/02/2020.
//  Copyright © 2020 City of London Consulting Limited. All rights reserved.
//

import UIKit
import sp_package1
import SP_COLCCommonCode

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //must be open
        let myClass = MyClass()
        
        //can be only public - not open
        myClass.doSomething()
        // if access not declared then its internal and we cant use it
        //myClass.doSomethingNotPublic()
        
    }


}

